import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './app';

import HomePage from 'pages/home/home';
import HomeRoute from 'pages/home/route';
import ListRoute from 'pages/list/route';
import ErrorPage from 'pages/error/index';

export default (
    <Route component={App} path='/'>
        <IndexRoute component={HomePage}/>

        {HomeRoute}
        {ListRoute}

        <Route path='*' component={ErrorPage} />
    </Route>
);