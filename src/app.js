import React from 'react';
import Header from './components/header';
import Modal from './components/modal/modal';
import DevTools from './utils/devtools';

export default class App extends React.Component {
    render() {
        return (
            <div>
                <Modal/>
                <Header/>

                {this.props.children}

                <DevTools/>
            </div>
        )
    }
}
