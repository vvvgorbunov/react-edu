import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

import HomeReducer from './pages/home/reducer';
import ListReducer from './pages/list/reducer';
import ModalReducer from './components/modal/reducer';

export default combineReducers({
    routing: routerReducer,
    ...HomeReducer,
    ...ListReducer,
    ...ModalReducer
})
