import React from 'react';
import classNames from 'classnames';

export default class Input extends React.Component {
    constructor(props) {
        super(props);

        const {value} = this.props;
        this.state = {value};
    }

    handelChange(event) {
        const {value} = event.target;
        this.props.onChange(value);
        this.setState({value});
    }


    render() {
        const divClasses = classNames({
           'form-group': true,
            'has-error': this.props.error ? true : false
        });

        const {value} = this.props;

        return (
            <div className={divClasses}>
                <input
                    type="text"
                    className="form-control"
                    value={value}
                    onChange={this.handelChange.bind(this)}
                />
                { this.props.error ? <span className="help-block">{this.props.error}</span> : null}
            </div>
        )
    }
}
