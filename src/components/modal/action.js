export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export function openModal(options) {
    return {
        type: OPEN_MODAL,options
    }
}

export function closeModal() {
    return {
        type: CLOSE_MODAL
    }
}