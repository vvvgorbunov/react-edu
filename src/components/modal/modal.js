import React from 'react';
import {connect} from 'react-redux';
import {closeModal} from './action'

class Modal extends React.Component {
    constructor(props) {
        super(props);
    }

    close() {
        this.props.dispatch(closeModal());
    }

    render() {
        const {isOpened, title, content} = this.props.modal;

        if (!isOpened) return null;

        return (
            <div className="modal fade in">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" onClick={this.close.bind(this)}><span>&times;</span></button>
                            <h4 className="modal-title">{title}</h4>
                        </div>
                        <div className="modal-body">
                            {content}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" onClick={this.close.bind(this)}>Close</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        modal: state.modal
    }
}

export default connect(mapStateToProps)(Modal);
