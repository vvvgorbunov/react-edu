import {OPEN_MODAL, CLOSE_MODAL} from './action'

const initialState = {
    title: '',
    content: '',
    isOpened: false
};

function modalReducer(state = initialState, action) {
    switch (action.type) {
        case OPEN_MODAL:
            return Object.assign({}, state, {
                isOpened: true,
                title: action.options.name,
                content: action.options.text
            });

        case CLOSE_MODAL:
            return Object.assign({}, state, {
                isOpened: false
            });

        default:
            return state;
    }
}

const ModalReducer = {
    modal: modalReducer
};

export default ModalReducer;