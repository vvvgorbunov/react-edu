import React from 'react';
import {Link} from 'react-router';

export default class Header extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <Link className="navbar-brand" href="/">Brand</Link>
                    </div>

                    <ul className="nav navbar-nav">
                        <li><Link to="/list">List</Link></li>
                        <li><Link to="/error">Error</Link></li>
                    </ul>
                </div>
            </nav>
        )
    }
}
