import React from 'react';
import ListItem from './list-item';
import {connect} from 'react-redux';
import {getItems} from './action';
import Loader from '../../components/ui/loader';

class ListPage extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            items: []
        };

        this.props.dispatch(getItems());
    }


    renderItems(item, idx) {
        return (
            <ListItem key={idx} id={idx} img={item.picture} name={item.name} desc={item.about} date={item.date}/>
        );
    }


    render() {
        const {items, isLoading} = this.props.list;
        
        return (
            <div className="container">
                <h1>List page</h1>

                <br/>
                <br/>

                {
                    isLoading ? <Loader /> : items.length !== 0 ?

                        <div>
                            {items.map(this.renderItems.bind(this))}
                        </div> :

                        'Элементов нет'
                }


            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        list: state.list
    }
}

export default connect(mapStateToProps)(ListPage);