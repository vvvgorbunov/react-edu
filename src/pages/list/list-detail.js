import React from 'react';
import {store} from '../../index';
import {connect} from 'react-redux';
import {openModal} from '../../components/modal/action';

class ListDetail extends React.Component {
    constructor(props) {
        super(props);

        const item = this.getCurrentItem();

        this.state = {
            id: item.id,
            name: item.name,
            date: item.date,
            text: item.text
        };
    }

    getCurrentItem() {
        const actualStore = store.getState();
        const {items} = actualStore.list;
        
        const idx = items.findIndex(item => item.id === this.props.routeParams.id);
        
        return (
            {
                name: items[idx].name,
                id: items[idx].id,
                text: items[idx].text,
                date: items[idx].date
            }
        );
    }

    open() {
        const options = {
            name: this.state.name,
            text: this.state.text
        };

        this.props.dispatch(openModal(options));
    }


    render() {
        return (
            <div className="container">
                <time>{this.state.date}</time>

                <br/>

                <h1>{this.state.name}</h1>

                <p>
                    {this.state.text}
                </p>

                <button className="btn btn-success" onClick={this.open.bind(this)}>Modal open</button>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {};
}

export default connect(mapStateToProps)(ListDetail);
