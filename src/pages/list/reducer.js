import {GET_ITEMS} from './action';

const initialState = {
    items: [],
    isLoading: true
};

function listReducer(state = initialState, action) {
    let items = state.items;

    switch (action.type) {
        case GET_ITEMS:
            return Object.assign({}, state, {
                items: action.items,
                isLoading: false
            });

        default:
            return state;
    }
}

const ListReducer = {
    list: listReducer
};

export default ListReducer;