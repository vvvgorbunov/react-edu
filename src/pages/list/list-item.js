import React from 'react';
import {Link} from 'react-router';

export default class ListItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="media">
                <div className="media-left">
                    <Link to={`/list/${this.props.id}`}>
                        <img className="media-object" src={this.props.img} alt="" />
                    </Link>
                </div>
                <div className="media-body">
                    <time>{this.props.date}</time>

                    <h4 className="media-heading">
                        <Link to={`/list/${this.props.id}`}>
                            {this.props.name}
                        </Link>
                    </h4>
                    <div>
                        {this.props.desc}
                    </div>
                </div>
            </div>
        )
    }
}
