export const GET_ITEMS = "GET_ITEMS";
import axios from 'axios';

export function getItems() {
    return (dispatch) => {
        axios.get('./src/serve/list.json').then(res => {
            let items = res.data;

            setTimeout(function () {
                dispatch({
                    type: GET_ITEMS,items
                })
            }, 2000);
        });
    }
}