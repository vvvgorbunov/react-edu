import React from 'react';
import {Route} from 'react-router';
import ListPage from './list';
import ListDetail from './list-detail';

export default (
    <Route>
        <Route component={ListPage} path='/list'/>
        <Route component={ListDetail} path='/list/:id'/>
    </Route>
)