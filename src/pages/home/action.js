export const ADD_TODO = "ADD_TODO";
export const LIKE_TODO = "LIKE_TODO";
export const DEL_TODO = "DEL_TODO";
export const GET_TODOS = "GET_TODOS";

import axios from 'axios';

export function addTodo(todos, name) {
    let error = '';

    if (!name) {
        error = 'Неообходимо ввести название'
    }

    let id = 1;

    if (todos.length) {
        id = todos[todos.length - 1].id + 1;
    }

    return {
        type: ADD_TODO, id, name, error
    }
}

export function likeTodo(item) {
    let liked = !item.liked;

    return {
        type: LIKE_TODO, item, liked
    }
}

export function delTodo(item) {
    return {
        type: DEL_TODO, item
    }
}

export function getTodos() {
    return (dispatch) => {
        axios.get('./src/serve/todos.json').then(res => {
            let todos = res.data;

            setTimeout(() => {
                dispatch({
                    type: GET_TODOS,todos
                })
            }, 2000);
        });
    }
}