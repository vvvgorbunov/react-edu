import React from 'react';
import {connect} from 'react-redux';
import {addTodo, likeTodo, delTodo, getTodos} from './action';
import classNames from 'classnames';
import Input from '../../components/ui/input/index';
import Loader from '../../components/ui/loader';

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            todoName: ''
        };

        this.props.dispatch(getTodos());
    }

    inputOnChange(value) {
        this.setState({todoName: value});
    }

    addTodo() {
        const {todos} = this.props.home;
        this.props.dispatch(addTodo(todos, this.state.todoName));

        this.setState({todoName: ''});
    }

    likeTodo(item) {
        this.props.dispatch(likeTodo(item));
    }

    delTodo(item) {
        this.props.dispatch(delTodo(item));
    }

    renderTodos(item, id) {
        return (
            <li key={id}>
                <button className="btn btn-danger" onClick={this.delTodo.bind(this, item)}>Del</button>
                <button className={classNames('btn btn-success', {'active': item.liked})}
                        onClick={this.likeTodo.bind(this, item)}>Like
                </button>

                <span>{item.name}</span>
            </li>
        );
    }

    render() {
        const {todoName} = this.state;
        const {todos, error, isLoading} = this.props.home;

        return (
            <div className="container">
                <h1>Home page</h1>

                <br/>
                <br/>

                {
                    isLoading ? <Loader /> : todos.length !== 0 ?

                        <ul className="todoList">
                            {todos.map(this.renderTodos.bind(this))}
                        </ul> :

                        'Элементов нет'
                }

                <div className="row form">
                    <div className="col-md-5">
                        <Input
                            onChange={this.inputOnChange.bind(this)}
                            value={todoName}
                            error={error}
                        />
                        <button className="btn btn-primary" onClick={this.addTodo.bind(this)}>Add todo</button>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        home: state.home
    }
}

export default connect(mapStateToProps)(HomePage);