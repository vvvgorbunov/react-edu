import {ADD_TODO,LIKE_TODO,DEL_TODO,GET_TODOS} from './action';

const initialState = {
    todos: [],
    error: '',
    isLoading: true
};

function homeReducer(state = initialState, action) {
    let todos = state.todos;

    switch (action.type) {
        case ADD_TODO:
            if (!action.error) {
                todos.push({
                    id: action.id,
                    name: action.name,
                    liked: false
                })
            }

            return Object.assign({}, state, {
                error: action.error,
                todos
            });

        case LIKE_TODO:
            let idLike = todos.findIndex(todo => todo.id === action.item.id);

            todos[idLike].liked = action.liked;

            return Object.assign({}, state, {todos});

        case DEL_TODO:
            todos = todos.filter(todo => todo.id !== action.item.id);

            return Object.assign({}, state, {todos});

        case GET_TODOS:
            return Object.assign({}, state, {
                todos: action.todos,
                isLoading: false
            });

        default:
            return state;
    }
}

const HomeReducer = {
  home: homeReducer
};

export default HomeReducer;