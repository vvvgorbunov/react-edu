import React from 'react';
import {Link} from 'react-router';

export default class ErrorPage extends React.Component {
    render() {
        return (
            <div>
                <h1>Error page 404</h1>
                <p>Go to <Link to="/">main page</Link></p>
            </div>
        )
    }
}
